#!/bin/bash
loc=$(pwd)

if [[ ! -d "$loc/forensic_log_website_daffainfo_log" ]]
then
	mkdir $loc/forensic_log_website_daffainfo_log 
fi

cat $loc/log_website_daffainfo.log | awk -F: '{gsub(/"/, "", $3)arr[$3]++} 
	END {
		for (i in arr) {
			if (i != "Request"){ 	
        req+=arr[i]
      }
		}
		req=req/12
		printf "rata rata serangan perjam adalah sebanyak %f request per jam", req
	}' >>  $loc/forensic_log_website_daffainfo_log/ratarata.txt

cat $loc/log_website_daffainfo.log | awk -F: '{gsub(/"/, "", $1)arr[$1]++}
	END {
		max=0
		target
		for (i in arr){
			if (max < arr[i]){
				target = i
				max = arr[target]
			}
		}
		print "yang paling banyak mengakses server adalah: " target " sebanyak " max " request"
	}' >>  $loc/forensic_log_website_daffainfo_log/result.txt

cat $loc/log_website_daffainfo.log | awk '/curl/ {++n} 
	END {
	print "ada " n " request dengan curl sebagai user-agent"
	}' >>  $loc/forensic_log_website_daffainfo_log/result.txt

cat $loc/log_website_daffainfo.log | awk -F: '/2022:02/ {gsub(/"/, "", $1)arr[$1]++}
	END {
		for (i in arr){
			print i
		}
	}' >> $loc/forensic_log_website_daffainfo_log/result.txt
