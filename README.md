
Praktikum Sistem Operasi (A)

Ferdinand Putra Gumilang Silalahi - 5025201176

Elbert Dicky Aristyo - 5025201231

Alif Adrian Anzary - 5025201274

Penjelasan soal no 1 :

- Untuk langkah pertama, kita akan membuat sistem register untuk membuat akun (register.sh) terlebih dahulu.
- Script register.sh akan menyimpan informasi login (username dan password) dalam file user.txt di dalam direktori soal1/users/user.txt.
- Input username dan password menggunakan command read, dengan argumen “-p” untuk menambahkan teks prompt pada input, pada saat input password harus tertutup/hidden untuk menjaga keamanan. Untuk memastikan password tertutup, tambahkan argumen “-s” pada command read untuk password.
- Untuk mengecek apabila password sama dengan username atau tidak, menggunakan conditional “$password == $username”.
- Untuk mengecek apabila password lebih dari atau kurang dari 8 karakter, akan menggunakan “$passlength -lt 8”.
- Untuk mengecek apakah password mengandung huruf kapital, kecil, dan nomor, akan menggunakan “"$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]*”.
- Setelah register berjalan, percobaan login yang terdiri dari username dan password akan di simpan di dalam soal1/log.txt dengan format $calendar $time message.
- Informasi calendar akan didapat dengan $(date +%D), dan informasi time akan didapat dengan $(date +%T).
- Jika saat register, tertulis user yang sudah pernah register (dengan cara mengecek didalam soal1/users/user.txt), maka akan dimasukkan register error.
- Jika register berhasil, dengan password yang tepat, maka akan dimasukkan register info sukses. 
- Untuk langkah kedua, kita akan membuat sistem login (main.sh).
- Script main.sh akan menyimpan informasi percobaan login yang berhasil/tidak dalam bentuk file log.txt yang berada pada direktori soal1/log.txt.
- Langkah pengecekan username dan password sama seperti penjelasan register.sh.
- Setelah login, kita membuat 2 fungsi yang itu start_dl dan att. command start_dl berfungsi untuk mendownload jumlah gambar yang kita inginkan, kita bisa menginput link gambar sesuai yang kita inginkan. setelah berhasil mendownload gambar, file gambar akan disimpan dalam file yang berbentuk zip pada direktori soal1, format nama file yang akan disimpan adalah YYYY-MM-DD_USERNAME dan format nama gambarnya PIC_XX. ketika kita mencoba akses zip nya kita akan di minta masukan password yang sesuai dengan username. sedangkan fungsi att adalah untuk menghintung jumlah percobaan login yang berhasil/tidak dari user yang sedang login saat ini. 

Penjelasan soal no.2
- untuk soal kedua, yang diperlukan adalah rata-rata request per jam yang akan dimasukkan ke ratarata.txt, IP paling banyak request server, banyak request yang menggunakan agen curl, dan daftar IP yang akses website pada jam 2 pagi, ketiganya dimasukkan ke result.txt
- Semua data tersebut akan dicari didalam $loc/log_website_daffainfo.log menggunakan fungsi cat pada awk, yang berfungsi membaca log.
- Akan diambil array yang diperlukan, seperti $1 (IP), $3 (request), dan lain-lain.
- Untuk request per jam, hasil hitungan loop akan dibagi 12, untuk mengecek setiap 5 menit.

Penjelasan Soal No 3

- Buat variabel direktori untuk menyimpan direktori pengerjaan (dalam kasus ini, ~/log)
- Gunakan if statement untuk membuat direktori ~/log menggunakan mkdir jika belum ada
- Fungsi create_log adalah fungsi yang mengeprint log-log yang diperlukan (free -m dan du -sh ~).
- Variabel berisi tanggal dibuat dan formatnya dipastikan sesuai (%Y%m%d%k%M%s).
- create_log di call dan dioutput ke file yang bernama $tanggal.log

