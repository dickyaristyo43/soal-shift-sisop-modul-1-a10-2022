directory=~/log

create_log()
{
	free -m
	printf "\n"
	du -sh ~
}	



if [[ ! -d "$directory" ]]
    then
	mkdir $directory
    fi


currDate=$(date +"%Y%m%d%k%M%S") 

create_log >> $directory/$currDate.log
