#!/bin/bash
calendar=$(date +%D)
time=$(date +%T)
folder=$(date +%Y-%m-%d)_$username
loc=$(pwd)

usernameCheck(){
	if [[ ! -f "$loc/users/user.txt" ]]
			then
		echo "No user registered yet"
    exit
    fi
  if grep -q $username "$loc/users/user.txt"
    	then
    echo "User found"
  else 
  	echo "User not found"
    exit
      fi
}

passwordCheck(){
	local passLength=${#password}

	if [[ $passLength -lt 8 ]]
    	then
    echo "Password must be more than 8 characters"
      
	elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
			then
    echo "Password must has at least upper-case, lower-case and number"
      
  else
		login
	fi
}

login(){
	checkUser=$(egrep $username "$loc/users/user.txt")
	checkPass=$(egrep $password "$loc/users/user.txt")

	if [[ -n "$checkUser" ]] && [[ -n "$checkPass" ]]
			then
		echo "$calendar $time LOGIN:INFO User $username logged in" >> $loc/log.txt
		echo "Login success"
		echo "Enter command [dl or att]: "
			read command
      	if [[ $command == att ]]
					then
				att
      	elif [[ $command == dl ]]
					then
				dl_pic
      	else
					echo "Not found"
				fi

	else
		echo "Failed login attempt on user $username"
		echo "$calendar $time LOGIN:ERROR Failed login attempt on User $username" >> $loc/log.txt
	fi
}

dl_pic(){
	echo "Enter Number: "
	read n

	if [[ ! -f "$folder.zip" ]]
	then
		mkdir $folder
		count=0
		start_dl
	else
		unzip -P $password $folder.zip
		rm $folder.zip

		count=$(find $folder -type f | wc -l)
		start_dl
	fi

}

start_dl(){
	for(( i=$count+1; i<=$n+$count; i++ ))
	do
		wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
	done

	zip --password $password -r $folder.zip $folder/
	rm -rf $folder
}

att(){
	if [[ ! -f "$loc/log.txt" ]]
	then
		echo "Log not found"
	else  
		count=$(grep -c "$username" $loc/log.txt)
		let "count=count-1"
    echo "$count"
	fi
}


read -p "Enter Username: " username

usernameCheck

read -p "Enter Password: " -s password

passwordCheck
