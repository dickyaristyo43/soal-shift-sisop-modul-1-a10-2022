#!/bin/bash
calendar=$(date +%D)
time=$(date +%T)
loc=$(pwd)

usernameCheck() {
if [[ ! -d "$loc/users" ]]
    then
	mkdir $loc/users
    fi

if grep -q $username "$loc/users/user.txt"
    then
	echo "User already exists"
	echo "$calendar $time REGISTER:ERROR User already exists" >> $loc/log.txt
	exit
    fi
}

passwordCheck() {
  local passLength=${#password}
  
    if [[ $password == $username ]]
    then
        echo "Password cannot be the same as username"

    elif [[ $passLength -lt 8 ]]
    then
        echo "Password must be more than 8 characters"

    elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
    then
	echo "Password must has at least upper-case, lower-case and number"

    else
      	echo "Register successful"
      	echo "$calendar $time REGISTER:INFO User $username registered successfully" >> $loc/log.txt
      	echo $username $password >> $loc/users/user.txt
    fi


}

read -p "Enter Username: " username

usernameCheck

read -p "Enter Password: " -s password

passwordCheck
